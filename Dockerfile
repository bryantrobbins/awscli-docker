FROM python:alpine

ARG AWSCLI_VERSION

RUN apk update && apk upgrade -U -a
RUN pip install awscli==$AWSCLI_VERSION